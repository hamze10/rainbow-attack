#include "attack.h"


std::vector<std::string> explode(std::string const & s, char delim)
{
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim); )
    {
        result.push_back(std::move(token));
    }

    return result;
}

void countNbFile(unsigned & nbFile, std::string & file){
	
	std::string line;
	std::ifstream file1(file);
    while (std::getline(file1, line))
        nbFile++;
	
}


bool recherche_dicho(unsigned & lines, std::string& tail, std::string& result, std::string& file, unsigned length){
    int indiceDebut = 0, indiceFin = lines-1, indiceMilieu;
    bool found = false;
    std::string candidat;
    std::ifstream inFile;
    std::string tmpTail = tail;

    inFile.open(file);
    std::vector<std::string> headQueue;
    while(!found && indiceDebut <= indiceFin){

        indiceMilieu = (indiceDebut + indiceFin)/2;
        inFile.seekg(indiceMilieu* (length * 2 + 3));
        std::getline(inFile, candidat);
        headQueue = explode(candidat, ';');
        if (headQueue.at(0) == tmpTail) {
            found = true;
        } else if (headQueue.at(0) < tmpTail ) {
            indiceDebut = indiceMilieu + 1;
        } else if (headQueue.at(0) > tmpTail) {
            indiceFin = indiceMilieu - 1 ;
        }
    }

    if(found){
        result = headQueue.at(1);
    }

    inFile.close();

    return found;
}

void attack(std::string hash, unsigned passwordSize, sqlite3 * db){
	bool found = false;
	sqlite3_stmt *stmt;
	std::string head;
	std::string a;

	
	
	int i = 10000;
	for (; i > 0; i--){
		a = repeatReductionfromHash(hash, 10001-i, i, passwordSize);
		sqlite3_prepare_v2(db, "SELECT HEAD FROM CHAINS WHERE TAIL = ?;", -1, &stmt, NULL);
		sqlite3_bind_text(stmt, 1, a.c_str(), -1, NULL);
		SHA256 sha;
		
		while (sqlite3_step(stmt) != SQLITE_DONE) {			
			head = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
			sha = SHA256();
			sha.add(repeatReduction(head, i-1, 1, passwordSize).c_str(), passwordSize);					
			if(hash == sha.getHashString()){
				found = true;
				break;
			}			
        };
		if (found){
			break;
		}
		sqlite3_finalize(stmt);
    }	
	if (found){
		std::string headFinal(head);
		std::cout << "The password is "  << repeatReduction(headFinal, i-1, 1, passwordSize) << std::endl;
	}
	else{
		std::cout << "The password for " << hash << " has not been found" << std::endl;
	}
}