#include <string>
#include "sqlite3.h"

int callback(void *NotUsed, int argc, char **argv, char **azColName);

int createTable(sqlite3 *db, int &rc,char *zErrMsg);

void pushChain(sqlite3_stmt *stmt, sqlite3 *db, int& rc,	char *zErrMsg);

int commit(sqlite3 *db, int &rc,char *zErrMsg);
