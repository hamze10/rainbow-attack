#include "reduction.h"
#include <iostream>    
#include <fstream>
#include <map>
#include <sstream>
#include <utility>
#include "dbAcess.h"
#include "sha256.h"

std::vector<std::string> explode(std::string const & s, char delim);
bool recherche_dicho(unsigned & lines, std::string& hash, std::string& result, std::string& file, unsigned length);
void attack(std::string hash, unsigned passwordSize, sqlite3 * db);
void countNbFile(unsigned & nbFile, std::string & file);