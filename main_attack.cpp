#include <iostream>
#include <vector>
#include <thread>
#include "attack.h"
#include "sqlite3.h"


int main(int argc, char* argv[]){
	
	sqlite3 *db;

    char *zErrMsg = 0;

    int rc;
	
	rc = sqlite3_open("Table.db", &db);
	
	std::string candidat;
	std::ifstream inFile;
    inFile.open("passwords.txt");
	std::vector<std::string> passwords;
    while(std::getline(inFile, candidat)){
        passwords.push_back(candidat);
	}
	
	std::thread first;
	std::thread second;
	std::thread third;
	std::thread fourth;
	unsigned int passwordSize = 8;
	for (unsigned i = 0; i<100; i = i+4){
			first = std::thread(attack, passwords.at(i),passwordSize, db);
			second = std::thread(attack, passwords.at(i+1),passwordSize, db);
			third = std::thread(attack, passwords.at(i+2),passwordSize, db);
			fourth = std::thread(attack, passwords.at(i+3),passwordSize, db);
			
			first.join();
			second.join();
			third.join();
			fourth.join();
		
	}
}