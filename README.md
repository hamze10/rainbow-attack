# Rainbow Attack

### Members of the group
- Hamza MAHMOUDI
- Elias EL HATHOUT
- Mohammad YASSINE


### Prerequisite 
	The hash that will be attacked needs to be stored in a file named "passwords.txt"


### Commands
- Compile the sqlite3 libraries : gcc -c -O3 sqlite3.c sqlite3.h


- Generate rainbow table : g++ -O3 -pthread -std=c++17 -o generate dbAcess.cpp dbAcess.h generate.cpp passwd-utils.hpp random.hpp reduction.cpp reduction.h sha256.h sha256.cpp sqlite3.o -ldl



- Attack the table : g++ -O3 -pthread -std=c++17 -o attack attack.cpp attack.h dbAcess.cpp dbAcess.h reduction.cpp reduction.h sha256.h sha256.cpp main_attack.cpp sqlite3.o -ldl