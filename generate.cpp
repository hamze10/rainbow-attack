#include <iostream>
#include <math.h>
#include "passwd-utils.hpp"
#include "reduction.h"
#include <vector>
#include <thread>
#include "attack.h"
#include "dbAcess.h"
#include "sha256.h"

std::string generate(int length, unsigned& nbRep, std::string& head) {

  std::string query = repeatReduction(head, nbRep, 1, length);
  return query;
}

void generateMultiple(int length, sqlite3 * db, int rc, char* zErrMsg){
	unsigned nbRep = 10000;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "INSERT INTO CHAINS (HEAD,TAIL) VALUES (?, ?)", -1, &stmt, NULL);
	std::string tail;
    for (unsigned i = 0; i<5280000; i++){		
    	std::string head = rainbow::generate_passwd(length);
		tail = generate(length, nbRep, head);
		sqlite3_bind_text(stmt, 1, head.c_str(), -1, NULL);
		sqlite3_bind_text(stmt, 2, tail.c_str(), -1, NULL);		
		pushChain(stmt,db,rc,zErrMsg);
	}
	sqlite3_finalize(stmt);	
}

int main() {


	int length;
	std::cout << "Enter the length of the passwords: " << std::endl;
	std::cin >> length;
	
    sqlite3 *db;

    // Save any error messages
    char *zErrMsg = 0;

    // Save the result of opening the file
    int rc;

    // Save any SQL
    char* sql;

    // Save the result of opening the file
    rc = sqlite3_open("Table.db", &db);
    
    if(rc){
        // Show an error message
        std::cout << "DB Error: " << sqlite3_errmsg(db) << std::endl;
        // Close the connection
        sqlite3_close(db);
    }
	createTable(db,rc,zErrMsg);
	std::thread first (generateMultiple,length, db , rc, zErrMsg);
	std::thread second (generateMultiple,length, db , rc, zErrMsg);
	std::thread third (generateMultiple,length, db , rc, zErrMsg);
	std::thread fourth (generateMultiple,length, db , rc, zErrMsg);
  
	first.join();
	second.join();
	third.join();
	fourth.join();
	commit(db,rc,zErrMsg);

}