#include <iostream>
#include "reduction.h"
#include <cstring>


using namespace std;

const char * characters="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

string convertToString(char* a, int size) 
{ 
    int i;
    string s = ""; 
    for (i = 0; i < size; i++) { 
        s = s + a[i]; 
    }
    return s; 
} 


std::string repeatReduction(string& password, int nbRep, int i, int passwordSize)
{
	std::string pass;
	pass.assign(password);
    std::string hash;
	SHA256 sha;
    for(int j = 1; j<= nbRep ;j++){
		sha = SHA256();
		sha.add(pass.c_str(), passwordSize);		
	    hash = sha.getHashString();
        reduction(hash, i, passwordSize, pass);
		i = i+1;
    }
    return pass;
}

void reduction(std::string& hash, int& i, int& passwordSize, string & pass)
{
    unsigned long long int convertedHash = strtoull(hash.substr(0,16).c_str(), NULL, 16) + i;
    pass = "";
    unsigned mod;
    for (unsigned k=0 ; k<passwordSize ; k++){
            mod = (convertedHash * 23) % 62;
            pass = characters[mod] + pass;
            convertedHash = convertedHash / 62;
    }
}

std::string repeatReductionfromHash(std::string& hash, int nbRep, int i, int passwordSize){
	std::string password;
    reduction(hash, i, passwordSize, password);
	return repeatReduction(password, nbRep-1, i+1, 8);
}