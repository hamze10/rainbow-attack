#include "dbAcess.h"

using namespace std;

// Create a callback function  
int callback(void *NotUsed, int argc, char **argv, char **azColName){
    // Return successful
    return 0;
}
  
int createTable(sqlite3 *db, int &rc,char *zErrMsg) {

    // Save SQL to create a table
    std::string sql = "CREATE TABLE CHAINS (HEAD TEXT NOT NULL, TAIL TEXT NOT NULL);";
    
    // Run the SQL (convert the string to a C-String with c_str() )
    rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	
	sql = "CREATE INDEX Tail_index ON CHAINS(TAIL);";
    rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	
	sql = "BEGIN TRANSACTION;";
	rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	
	return rc;
    
}

void pushChain(sqlite3_stmt *stmt, sqlite3 *db, int &rc,	char *zErrMsg){
	sqlite3_step(stmt);
	sqlite3_reset(stmt);
}

int commit(sqlite3 *db, int &rc,char *zErrMsg){
	std::string sql = "COMMIT;";
    
    rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	
	return rc;
}