#ifndef REDUCTION_H
#define REDUCTION_H
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "sha256.h"


std::string repeatReduction(std::string& password, int nbRep, int i, int passwordSize);
std::string repeatReductionfromHash(std::string& hash, int nbRep, int i, int passwordSize);
void reduction(std::string& hash, int& i, int& passwordSize, std::string& pass);


#endif // REDUCTION_H
